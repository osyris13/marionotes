<?php
	include('GoogleAuthentication/validate.php');
?>
<html>
	<head>
		<link rel="stylesheet" href="lib/codemirror.css">
		<link rel="stylesheet" href="theme/monokai.css">
        <script src="lib/codemirror.js"></script>
        <script src="addon/edit/matchbrackets.js"></script>
        <script src="addon/mode/loadmode.js"></script>
		<style>
			body {
				padding: 0;
				margin: 0;
				font-family: Arial, 'Helvetica Neue', Helvetica, sans-serif;
			}
			#wrapper {
				width: 100%;
				height: 100%;
			}

			header, footer {
				width: 100%;
				height: 40px;
				background-color: #000000;
				color: #ffffff;
			}

			#notebookList, #noteList, #note {
				background-color: #ffffff;
				float: left;
				height: calc(100% - 40px - 40px);
			}

			#notebookList {
				background-color: #dedede;
				width: 200px;
			}

			#notebookList ol.menu {
				width: 100%;
				padding: 0;
				margin: 0;
				list-style: none;
			}

			#notebookList ol.menu li {
				height: 30px;
				border-bottom: 1px solid #000000;
				width: 100%;

			}

			h3 {
				padding: 0px;
				margin: 0px;
			}

			#notebookList ol.menu li a {
				display: block;
				padding-left: 10px;
				color: #000000;
				text-decoration: none;
				height: 30px;
				width: 100%;
				padding-top: 3px;
			}

			#notebookList ol.menu li a img {
				vertical-align: middle;
				padding-right: 10px;
				padding-left: 5px;
			}

			#notebookList ol.menu li:hover {
				background-color: #ffffff;
			}

			#noteList {
				background-color: #ffffff;
				border-left: 1px solid #000;
				border-right: 1px solid #000;
				width: 349px;
			}

			#noteList ol.menu {
				width: 100%;
				padding: 0;
				margin: 0;
				list-style: none;
			}

			#noteList ol.menu li {
				height: 70px;
				border-bottom: 1px solid #000000;
				width: 100%;

			}

			#noteList ol.menu li a {
				display: block;
				padding-left: 10px;
				color: #000000;
				text-decoration: none;
				height: 30px;
				width: 100%;
				padding-top: 3px;
				padding-right: 3px;
			}

			#noteList ol.menu li a img {
				vertical-align: middle;
				padding-right: 10px;
				padding-left: 5px;
			}

			#noteList ol.menu li:hover {
				background-color: #dedede;
			}

			#note {
				width: calc(100% - 551px);
				background-color: #dedede;
			}

			.summary em {
				font-size: 10px;
			}

			.summary {
				font-size: 12px;
			}

			input.search {
				display: block;
				float: right;
				width: 300px;
				padding: 5px;
				padding-right: 40px;
				outline: none;
				border-radius: 25px;
				font-size: 16px;
				border: 2px solid #999999;
				background-image: url('images/search.png');
				background-position: 260px 1px;
				background-repeat: no-repeat;
				margin-right: 20px;
				margin-top: 3px;
			}

			span.logo {
				font-size: 26px;
				font-weight: bold;
				background: -webkit-linear-gradient(#eee, #333);
				  -webkit-background-clip: text;
				  -webkit-text-fill-color: transparent;
			}

			.CodeMirror {
				height: calc(100% - 40px - 40px);
				font-family: "Lucida Console", Monaco, monospace;
				font-size: 12px;
			}

			.editorConfig {
				background-color: #dedede;
				height: 40px;
			}

			#editorHeader {

			}

			#editorFooter {
				
			}
		</style>
	</head>
	<body>
		<input type="hidden" id="hdnLanguage" />
		<div id="wrapper">
			<header>
				<img src="images/logo.png" style="padding:4px; vertical-align: middle" />
				<span class="logo">marioNotes</span>
				<input type="text" class="search" placeholder="Search" />
			</header>
			<div id="container">
				<div id="notebookList">
					<ol class="menu">
						<li><a href=""><img src="images/folio.png" />All Notes</a></li>
						<li><a href=""><img src="images/notebook.png" />Work</a></li>
						<li><a href=""><img src="images/notebook.png" />Code Grinder</a></li>
						<li><a href=""><img src="images/notebook.png" />Personal</a></li>
					</ol>
				</div>
				<div id="noteList">
					<ol class="menu">
						<li><a href=""><h3>Note Title</h3><span class="summary"><em>7 days ago:</em><br />This is a longer snippetasdf asdf asf adsf of my noasdf asdf asdf asdfasdf asd te...</span></a></li>
						<li><a href=""><h3>Note Title</h3><span class="summary"><em>7 days ago:</em><br />This is a longer snippet of my note...</span></a></li>
						<li><a href=""><h3>Note Title</h3><span class="summary"><em>7 days ago:</em><br />This is a snippet of my note...</span></a></li>
					</ol>
				</div>
				<div id="note">
					<div class="editorConfig" id="editorHeader">
						Welcome <?php echo $user_name; ?>
						<a href="index.php?reset=1">LogOut</a>
					</div>
<textarea id="code" name="code" style="height: 100%;">
&nbsp;
</textarea>
					<div class="editorConfig" id="editorFooter">
						<div class="option">
                            <label for="select-theme">Theme</label>
                            <select id="select-theme">
                                <option>default</option>
                                <option>3024-day</option>
                                <option>3024-night</option>
                                <option>ambiance</option>
                                <option>base16-dark</option>
                                <option>base16-light</option>
                                <option>blackboard</option>
                                <option>cobalt</option>
                                <option>colorforth</option>
                                <option>dracula</option>
                                <option>eclipse</option>
                                <option>elegant</option>
                                <option>erlang-dark</option>
                                <option>lesser-dark</option>
                                <option>liquibyte</option>
                                <option>material</option>
                                <option>mbo</option>
                                <option>mdn-like</option>
                                <option>midnight</option>
                                <option selected="selected">monokai</option>
                                <option>neat</option>
                                <option>neo</option>
                                <option>night</option>
                                <option>paraiso-dark</option>
                                <option>paraiso-light</option>
                                <option>pastel-on-dark</option>
                                <option>rubyblue</option>
                                <option>seti</option>
                                <option>solarized dark</option>
                                <option>solarized light</option>
                                <option>the-matrix</option>
                                <option>tomorrow-night-bright</option>
                                <option>tomorrow-night-eighties</option>
                                <option>ttcn</option>
                                <option>twilight</option>
                                <option>vibrant-ink</option>
                                <option>xq-dark</option>
                                <option>xq-light</option>
                                <option>yeti</option>
                                <option>zenburn</option>
                            </select>
                        </div>

                        <div class="option">
                            <label for="select-mode">Syntax</label>
                            <select id="select-mode">
                                <option value="apl">APL</option>
                                <option value="asciiarmor">asciiarmor</option>
                                <option value="asterisk">Asterisk</option>
                                <option value="clike">CLike</option>
                                <option value="clojure">Clojure</option>
                                <option value="cmake">CMake</option>
                                <option value="cobol">Cobol</option>
                                <option value="coffeescript">CoffeeScript</option>
                                <option value="commonlisp">Common Lisp</option>
                                <option value="css">CSS</option>
                                <option value="cypher">Cypher</option>
                                <option value="d">D</option>
                                <option value="dart">Dart</option>
                                <option value="diff">diff</option>
                                <option value="dtd">DTD</option>
                                <option value="dylan">Dylan</option>
                                <option value="ebnf">EBNF</option>
                                <option value="ecl">ECL</option>
                                <option value="eiffel">Eiffel</option>
                                <option value="erlang">Erlang</option>
                                <option value="forth">Forth</option>
                                <option value="fortran">Fortran</option>
                                <option value="gas">Gas</option>
                                <option value="gherkin">Gherkin</option>
                                <option value="go">Go</option>
                                <option value="groovy">Groovy</option>
                                <option value="haml">HAML</option>
                                <option value="htmlmixed">HTML</option>
                                <option value="http">HTTP</option>
                                <option value="idl">IDL</option>
                                <option value="jade">Jade</option>
                                <option value="javascript">JavaScript</option>
                                <option value="jinja2">Jinja2</option>
                                <option value="julia">Julia</option>
                                <option value="kotlin">Kotlin</option>
                                <option value="livescript">LiveScript</option>
                                <option value="lua">Lua</option>
                                <option value="markdown">Markdown</option>
                                <option value="mathematica">Mathematica</option>
                                <option value="mirc">mIRC</option>
                                <option value="mllike">mllike</option>
                                <option value="modelica">Modelica</option>
                                <option value="mumps">Mumps</option>
                                <option value="nginx">Nginx</option>
                                <option value="ntriples">Ntriples</option>
                                <option value="pascal">Pascal</option>
                                <option value="pegjs">PEG.js</option>
                                <option value="perl">PERL</option>
                                <option value="php">PHP</option>
                                <option value="pig">Pig</option>
                                <option value="plain text" selected="selected">Plain Text</option>
                                <option value="properties">Properties files</option>
                                <option value="puppet">Puppet</option>
                                <option value="python">Python</option>
                                <option value="q">Q</option>
                                <option value="r">R</option>
                                <option value="ruby">Ruby</option>
                                <option value="rust">Rust</option>
                                <option value="sass">SASS</option>
                                <option value="scheme">Scheme</option>
                                <option value="shell">Shell</option>
                                <option value="sieve">Sieve</option>
                                <option value="slim">Slim</option>
                                <option value="smalltalk">Smalltalk</option>
                                <option value="smarty">Smarty</option>
                                <option value="solr">Solr</option>
                                <option value="soy">Soy</option>
                                <option value="sparql">SparQL</option>
                                <option value="spreadsheet">Spreadsheet</option>
                                <option value="sql">SQL</option>
                                <option value="stex">Stex</option>
                                <option value="stylus">Stylus</option>
                                <option value="swift">Swift</option>
                                <option value="tcl">Tcl</option>
                                <option value="textile">Textile</option>
                                <option value="tiddlywiki">Tiddlywiki</option>
                                <option value="tiki">Tiki</option>
                                <option value="toml">TOML</option>
                                <option value="troff">Troff</option>
                                <option value="ttcn">TTCN</option>
                                <option value="ttcn-cfg">TTCN-CFG</option>
                                <option value="turtle">Turtle</option>
                                <option value="twig">Twig</option>
                                <option value="vb">VB.NET</option>
                                <option value="vbscript">VBScript</option>
                                <option value="velocity">Velocity</option>
                                <option value="verilog">Verilog</option>
                                <option value="xml">XML</option>
                                <option value="xquery">XQuery</option>
                                <option value="yaml">YAML</option>
                                <option value="z80">Z80</option>
                            </select>
                        </div>
					</div>
				</div>
			</div>
			<br style="clear:both" />
			<footer>
				<span>Mario Notes</span>
			</footer>
		</div>
		<script src="js/vendor/jquery-2.1.4.min.js"></script>
		<script>
            var editor = CodeMirror.fromTextArea(document.getElementById("code"), {
                lineNumbers: true,
                matchBrackets: true,
                mode: "php",
                indentUnit: 4,
                indentWithTabs: true,
                theme: "monokai"
            });
            editor.setSize('100%');
            var input = $('#select-theme');
            function selectTheme() {
	            theme = input.val();
	            editor.setOption("theme", theme);
	            $('head').append('<link rel="stylesheet" href="theme/' + theme + '.css">')
	        }

	        $('#select-theme').on('change', function(){
	            selectTheme();
	        })

	        selectTheme();


	        var mode = $('#select-mode').val();
	        function change() {
	            if (mode) {
	                editor.setOption("mode", mode);
	                CodeMirror.autoLoadMode(editor, mode);
	            } else {
	                alert("Could not find a mode corresponding to " + mode);
	            }
	        }

	        $('#select-mode').on('change', function() {
	            CodeMirror.modeURL = "mode/%N/%N.js";
	            mode = $(this).val();
	            $('#hdnLanguage').val(mode);
	            change();
	        })
        </script>
	</body>
</html>