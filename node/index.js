var app = require('express')();
var http = require('http').Server(app);
var io = require('socket.io')(http);
debug=true;
io.on('connection', function(socket, room){
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
    
    socket.on('chat message', function(data){
        emitToRoom(data);
    });

    socket.on('subscribe', function(data) {
        joinRoom(data);
    });

    socket.on('language mode', function(data) {
        if(debug) {
            console.log('a user - ' + socket.id + ', sent a language mode in ' + data.room + '@@' + data.message + "@@");
        }
        io.to(data.room).emit('language mode', { message: data.message, fromU: data.fromU });
    });

    socket.on('code edit', function(data) {
        io.to(data.room).emit('code edit', { message: data.message, fromLine: data.fromLine, fromCh: data.fromCh, toLine: data.toLine, toCh: data.toCh, fromU: data.fromU });
        
        if(debug) {
            console.log('a user - ' + socket.id + ', sent a code edit in ' + data.room + '@@' + data.message + "@@");
        }
    });

    socket.on('all code', function(data) {
        io.to(data.room).emit('all code', { message: data.message });
        
        if(debug) {
            console.log('receiving all code: ' + data.message);
            console.log('finished sending all code back');
        }
    });

    function joinRoom(data) {
        socket.join(data.room);
        emitToSocket(socket.id, 'socket Id', socket.id);
        emitToSocket(Object.keys(io.sockets.adapter.rooms[data.room])[0], 'send all', null);

        if(debug) {
            console.log('a user - ' + socket.id + ', connected to ' + data.room);
            console.log('sending send all to:' + Object.keys(io.sockets.adapter.rooms[data.room])[0]);
        }
    }

    function emitToRoom(data) {
        io.to(data.room).emit('chat message', data.message);
        if(debug) {
            console.log('sending message to ' + data.room);
        }
    }

    function emitToSocket(socketId, type, data) {
        io.sockets.connected[socketId].emit(type, data);
    }
});

http.listen(3000, function(){
  console.log('listening on *:3000');
});

function dump(obj) {
    var out = '';
    for (var i in obj) {
        out += i + ": " + obj[i] + "\n";
    }
    console.log(out);
}


