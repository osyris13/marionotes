module.exports = function(grunt) {
    // Vars
    var skinDir = 'skin/frontend/<%= pkg.custom_package_name %>/<%= pkg.custom_theme_name %>/';
    var appDir = 'app/design/frontend/<%= pkg.custom_package_name %>/<%= pkg.custom_theme_name %>/';

    // comment inside of wrapper
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        watch: {
            options: {
                livereload: true
            },
            sass: {
                files: ['scss/**/*.{scss,sass}'],
                tasks: ['sass:dev','autoprefixer:dev']
            },
            livereload: {
                files: [
                    skinDir + 'scss/{,*/}*.scss'
                ],
                options: {
                    livereload: true
                }
            }
        },
        sass: {
            dev: {
                options: {
                    sourceMap: true,
                    outputStyle: 'expanded'
                },
                files: {
                    'css/<%= pkg.custom_package_name %>.css': 'scss/<%= pkg.custom_package_name %>.scss'
                },

            },
            dist: {
                options: {
                    sourceMap: false,
                    outputStyle: 'compressed'
                },
                files: {
                    'css/<%= pkg.custom_package_name %>.css': 'scss/<%= pkg.custom_package_name %>.scss'
                }
            }
        },
        autoprefixer: {
            dev: {
                options: {
                    map: true,
                    browsers: [ "last 2 versions", "ie 7", "ie 8", "ie 9" ]
                },
                files: {
                    'css/<%= pkg.custom_package_name %>.css': 'css/<%= pkg.custom_package_name %>.css'
                }
            },
            dist: {
                options: {
                    map: false,
                    browsers: [ "last 2 versions", "ie 7", "ie 8", "ie 9" ]
                },
                files: {
                    'css/<%= pkg.custom_package_name %>.css': 'css/<%= pkg.custom_package_name %>.css'
                }
            }
        }
    });

    // Define Tasksets
    grunt.registerTask('default', [
        'sass:dev',
        'autoprefixer:dev',
        'watch'
    ]);
    grunt.registerTask('develop', [
        'sass:dev',
        'watch'
    ]);
    grunt.registerTask('production-compile', [
        'sass:dist',
    ]);

    grunt.loadNpmTasks('grunt-sass');
    grunt.loadNpmTasks('grunt-autoprefixer');
    grunt.loadNpmTasks('grunt-contrib-watch');
    grunt.loadNpmTasks('grunt-exec');
};
