-- phpMyAdmin SQL Dump
-- version 4.2.5
-- http://www.phpmyadmin.net
--
-- Host: localhost:3306
-- Generation Time: Jul 14, 2015 at 05:08 PM
-- Server version: 5.5.38
-- PHP Version: 5.3.28

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `CodeGrinder`
--
CREATE DATABASE IF NOT EXISTS `CodeGrinder` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `CodeGrinder`;

-- --------------------------------------------------------

--
-- Table structure for table `Nuggets`
--

DROP TABLE IF EXISTS `Nuggets`;
CREATE TABLE `Nuggets` (
  `NuggetId` int(11) NOT NULL,
  `Code` varchar(8) NOT NULL,
  `OwnerUserId` int(11) NOT NULL,
  `Content` text NOT NULL,
  `LanguageMode` varchar(50) NOT NULL,
  `DateCreated` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DateUpdated` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00'
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `Nuggets`
--

INSERT INTO `Nuggets` (`NuggetId`, `Code`, `OwnerUserId`, `Content`, `DateCreated`, `DateUpdated`) VALUES
(1, 'OYi9q12L', 0, 'this is my test ', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'A0nYlzuN', 0, '<?php\r\nerror_reporting(E_ALL);\r\nini_set(''display_errors'', ''On'');\r\n$pageContent = "";\r\n$formAction = "save.php";\r\n$pageCode = ''0'';\r\nif(isset($_GET[''id''])) {\r\n	require_once(''DB/Nugget.class.php'');\r\n	$currentNugget = new Nugget();\r\n	$currentNuggetData = $currentNugget->loadNugget($_GET[''id'']);\r\n	var_dump($currentNuggetData);\r\n	$pageContent = $currentNuggetData[0][''Content''];\r\n	$pageCode = $currentNuggetData[0][''Code''];\r\n}\r\n?>\r\n', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Qf5Mqzcn', 0, '\r\n<title>Demac Media codeGrinder</title>\r\n		<meta charset="utf-8"/>\r\n		<!-- <link rel="stylesheet" href="theme/monokai.css"> -->\r\n\r\n		<link rel="stylesheet" href="lib/codemirror.css">\r\n		<script src="lib/codemirror.js"></script>\r\n		<script src="addon/edit/matchbrackets.js"></script>\r\n		<script src="mode/htmlmixed/htmlmixed.js"></script>\r\n		<script src="mode/xml/xml.js"></script>\r\n		<script src="mode/javascript/javascript.js"></script>\r\n		<script src="mode/css/css.js"></script>\r\n		<script src="mode/clike/clike.js"></script>\r\n		<script src="mode/php/php.js"></script>\r\n		<style type="text/css">.CodeMirror {border-top: 1px solid black; border-bottom: 1px solid black;}</style>\r\n\r\nthis is my test yo!\r\nthis is my second test\r\ni love this!', '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'gua5xfTa', 0, '\r\nprotected function loadByColumn($column, $val) {\r\n			$sql = "select * from " . $this->_tableName . " where $column = ''" . $val . "''";\r\n			$this->_saveMode = "update";\r\n			return $this->marioReadQuery($sql);\r\n		}\r\n\r\n		protected function delete() {\r\n			$sql = "delete from " . $this->getTableName() . " where " . $this->_primaryKey . " = ''" . $this->_columnValues[$this->_primaryKey] . "''";\r\n			$this->marioQuery($sql);\r\n		}\r\n\r\n		protected function __construct() {\r\n			$this->_databaseHost = "localhost";\r\n			$this->_databaseUser = "Grinder";\r\n			$this->_databasePassword = "Grinder";\r\n			$this->_databaseName = "CodeGrinder";\r\n			$this->_databasePort = "8889";\r\n\r\n			$this->_saveMode = "insert";\r\n		}', '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `Users`
--

DROP TABLE IF EXISTS `Users`;
CREATE TABLE `Users` (
  `UserId` int(11) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `FirstName` varchar(100) NOT NULL,
  `LastName` varchar(100) NOT NULL,
  `Password` varchar(100) NOT NULL,
  `PasswordHash` varchar(100) NOT NULL,
  `PasswordSalt` varchar(100) NOT NULL,
  `LastLoginDate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `Nuggets`
--
ALTER TABLE `Nuggets`
 ADD PRIMARY KEY (`NuggetId`);

--
-- Indexes for table `Users`
--
ALTER TABLE `Users`
 ADD PRIMARY KEY (`UserId`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `Nuggets`
--
ALTER TABLE `Nuggets`
MODIFY `NuggetId` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
