<?php
    require_once('MarioDal.class.php');
    class User extends MarioDal {
        protected function __construct() {
            parent::__construct();

            // Define table properties
            $this->_columnValues = array('UserId' => 0, 'google_id' => '', 'google_name' => 0, 'google_email' => '', 'google_link' => '', 'google_picture_link' => '', 'refresh_token' => '');
            $this->_tableName = 'google_users';
            $this->_primaryKey = 'UserId';
        }
    }

