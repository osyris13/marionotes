<?php
/*
 * Copyright 2011 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
include_once "templates/base.php";
session_start();

require_once realpath(dirname(__FILE__) . '/../src/autoload.php');

/************************************************
ATTENTION: Fill in these values! Make sure
the redirect URI is to this page, e.g:
http://localhost:8080/user-example.php
************************************************/
$client_id = '992639596739-21bjsrum98dr0b46lnh1199d6mgp5o1b.apps.googleusercontent.com';
$client_secret = 'dnL1h0gUGTPk854RCHQOOw57';
$redirect_uri = 'http://notes.demacmedia.com/examples/user-example.php';

$client = new Google_Client();
$client->setClientId($client_id);
$client->setClientSecret($client_secret);
$client->setRedirectUri($redirect_uri);
$client->setScopes(array('email', 'profile'));
$client->setAccessType('offline');
$client->setApprovalPrompt('force');


/************************************************
If we're logging out we just need to clear our
local access token in this case
************************************************/
if (isset($_REQUEST['logout'])) {
    unset($_SESSION['access_token']);
}

/************************************************
If we have a 'code' back from the OAuth 2.0 flow,
we need to exchange that with the authenticate()
function. We store the resultant access token
bundle in the session, and redirect to ourself.
************************************************/
if (isset($_GET['code'])) {
    $test = $client->authenticate($_GET['code']);
    // save the refresh token here
    //
    foreach (json_decode($test) as $key => $value) {
        var_dump($key);
    }
    $_SESSION['access_token'] = $client->getAccessToken();
    $redirect = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['PHP_SELF'];
    header('Location: ' . filter_var($redirect, FILTER_SANITIZE_URL));
}

/************************************************
  If we have an access token, we can make
  requests, else we generate an authentication URL.
 ************************************************/
if (isset($_SESSION['access_token']) && $_SESSION['access_token']) {
  $client->setAccessToken($_SESSION['access_token']);
  $token_data = $_SESSION['access_token'];
  if($client->isAccessTokenExpired()) {
    echo "hahaha";
    error_log("Access token is expired. Trying refresh");
    $client->refreshToken('X/XXXXXX');
  }
} else {
  $authUrl = $client->createAuthUrl();
}



/************************************************
  If we're signed in we can go ahead and retrieve
  the ID token, which is part of the bundle of
  data that is exchange in the authenticate step
  - we only need to do a network call if we have
  to retrieve the Google certificate to verify it,
  and that can be cached.
 ************************************************/
if ($client->getAccessToken()) {
  $_SESSION['access_token'] = $client->getAccessToken();
  $user                 = $google_oauthV2->userinfo->get();
  $user_id              = $user['id'];
  $user_name            = filter_var($user['name'], FILTER_SANITIZE_SPECIAL_CHARS);
  $email                = filter_var($user['email'], FILTER_SANITIZE_EMAIL);
  $profile_url          = filter_var($user['link'], FILTER_VALIDATE_URL);
  $profile_image_url    = filter_var($user['picture'], FILTER_VALIDATE_URL);
  $personMarkup         = "$email<div><img src='$profile_image_url?sz=50'></div>";
}

echo pageHeader("User Query - Retrieving An Id Token");
if (strpos($client_id, "googleusercontent") == false) {
  echo missingClientSecretsWarning();
  exit;
}
?>
<div class="box">
  <div class="request">
<?php
if (isset($authUrl)) {
  echo "<a class='login' href='" . $authUrl . "'>Connect Me!</a>";
} else {
  echo "<a class='logout' href='?logout'>Logout</a>";
}
?>
  </div>

  <div class="data">

<?php 
echo $user              . "<BR>";
echo $user_id           . "<BR>";
echo $user_name         . "<BR>";
echo $email             . "<BR>";
echo $profile_url       . "<BR>";
echo $profile_image_url . "<BR>";
echo $personMarkup      . "<BR>";
if (isset($token_data)) {
  var_dump($token_data);
}
?>
  </div>
</div>
<?php
echo pageFooter(__FILE__);
